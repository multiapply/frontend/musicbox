## Spotifu
- An spotify UI clone

### Objective
- Train CSS Techniques

### Skills
- CSS Flexbox Layout
- CSS Architecture
- Accessibility
- Layout Composition
- CSS Imports